@echo off

if not exist qt-tmp.tar (
  copy /y "qt-everywhere-opensource-src-5.15.8.tar.xz" qt-tmp.tar.xz >nul
  if ERRORLEVEL 0 (
    c:\SoftwareDevelopment\cygwin64\bin\xz.exe -d qt-tmp.tar.xz
  )
)
if ERRORLEVEL 0 (
  c:\SoftwareDevelopment\cygwin64\bin\tar.exe xf qt-tmp.tar
  if ERRORLEVEL 0 (
    ren qt-everywhere-src-5.15.8 qt
  )
)
