#!/bin/bash

# Parameter checking.
if [ -z "$1" ]; then echo -e "\nError: missing Qt version." && exit 1; fi
if [ -z "$2" ]; then echo -e "\nError: missing architecture." & exit 1; fi

echo "Building qt-$1-$2 library."

echo "- removing old build."
rm -rf .build 1>/dev/null 2>&1

echo "- extracting source."
mkdir .build
if [ "$?" -eq "0" ]; then
  cd "$1"
  if [ "$?" -eq "0" ]; then
    if [ ! -d "qt" ]; then
      if [ "$1" == "5.12.6" ]; then
        tar xJf "qt-everywhere-src-$1.tar.xz"
        if [ "$?" -eq "0" ]; then
          mv "qt-everywhere-src-$1" qt
        fi
      else
        echo -e "\nError: unsupported version."
        cd ..
        exit 1
      fi
    fi
  fi
fi
if [ "$?" -ne "0" ]; then
  echo -e "\nError: extraction."
  PWD=$(pwd)
  cd "${PWD%$1}"
  exit 1
fi

echo - configuring build.
cp -f configure.sh ../.build >/dev/null
if [ "$?" -eq "0" ]; then
  cd "../.build"
  if [ "$?" -eq "0" ]; then
    ./configure.sh $1 $2
  fi
fi
if [ "$?" -ne "0" ]; then
  echo -e "\nError: configuration."
  PWD=$(pwd)
  cd "${PWD%$1}"
  exit 1
fi

echo "- building library."
make -j2
if [ "$?" -eq "0" ]; then
  echo "- installing library."
  make install
fi

cd ..
